# ufldl

A workthrough of the Unsupervised Feature Learning/Deep Learning tutorial from Stanford
http://deeplearning.stanford.edu/tutorial/

## License

Copyright © 2015 Reese Murdock

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
